resource "aws_iam_role" "dev_soar_es_cluster" {
  name = "DEV-SOAR-ES-CLUSTER"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.dev_soar_es_cluster.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.dev_soar_es_cluster.name
}

resource "aws_eks_cluster" "dev_soar_es_cluster" {
  name     = "DEV-SOAR-ES-CLUSTER"
  role_arn = aws_iam_role.dev_soar_es_cluster.arn

  vpc_config {
    subnet_ids = ["subnet-0cb72a3634dacbb2e", "subnet-019474e140741d209"]
  }

  tags = {
    Name = "DEV-SOAR-ES-CLUSTER"
  }
}

resource "aws_iam_role" "eks_nodes_dev_soar_es_cluster" {
  name = "DEV-SOAR-ES-CLUSTER-NODE-GROUP"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_nodes_dev_soar_es_cluster.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_nodes_dev_soar_es_cluster.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_nodes_dev_soar_es_cluster.name
}

resource "aws_eks_node_group" "node_dev_soar_es_cluster" {
  cluster_name    = aws_eks_cluster.dev_soar_es_cluster.name
  node_group_name = "DEV-SOAR-ES-CLUSTER-NODE-GROUP"
  node_role_arn   = aws_iam_role.eks_nodes_dev_soar_es_cluster.arn
  subnet_ids      = ["subnet-0cb72a3634dacbb2e", "subnet-019474e140741d209"]

  disk_size       = 50
  instance_types  = ["t3.2xlarge"]
  scaling_config {
    desired_size = 3
    max_size     = 3
    min_size     = 3
  }
  remote_access {
    ec2_ssh_key = "test-pair"
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]
}