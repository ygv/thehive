output "eks_cluster_endpoint" {
  value = aws_eks_cluster.dev_soar_cassandra_cluster.endpoint
}

output "eks_cluster_certificate_authority" {
  value = aws_eks_cluster.dev_soar_cassandra_cluster.certificate_authority 
}